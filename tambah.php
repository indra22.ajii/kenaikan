<?php

require 'koneksi.php';

date_default_timezone_set('Asia/Jakarta');

if(isset($_POST['save_dokumen']))
{
    $nip   = $_POST['nip'];
    $nodokumen = $_POST['no_dokumen'];
    $rand = rand();
    $filename = $_FILES['file']['name'];
    $status = $_POST['status'];
    $waktu = date('Y-m-d H:i:s');;

    if(isset($_FILES['file']['name'])) {
    $cpath="file/";
    $file_parts = pathinfo($_FILES["file"]["name"]);
    $file_path = 'resume'.time().'.'.$file_parts['extension'];
    move_uploaded_file($_FILES["file"]["tmp_name"], $cpath.$file_path);
    $cv2 = $file_path;
    }
    // move_uploaded_file($_FILES['file']['tmp_name'], 'file/'.$rand.'_'.$filename);
	//     $nama_dokumen = $rand.'_'.$filename;

    if($nip == NULL || $nodokumen == NULL || $filename == NULL || $status == NULL)
    {
        $res = [
            'status' => 422,
            'message' => 'All fields are mandatory'
        ];
        echo json_encode($res);
        return;
    }

    $query = "INSERT INTO tabel_dok (nip,no_dokumen,nama_dokumen,status,tgl_masuk) VALUES ('$nip','$nodokumen','$filename','$status','$waktu')";
    $query_run = mysqli_query($kon, $query);

    if($query_run)
    {
        $res = [
            'status' => 200,
            'message' => 'Dokumen Berhasil Dibuat'
        ];
        echo json_encode($res);
        return;
    }
    else
    {
        $res = [
            'status' => 500,
            'message' => 'Dokumen Tidak Dibuat'
        ];
        echo json_encode($res);
        return;
    }
}

?>